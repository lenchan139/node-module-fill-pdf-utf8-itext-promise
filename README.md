A promise wrapper for package [fill-pdf-utf8](https://www.npmjs.com/package/fill-pdf-utf8).
It uses iText so utf-8 support is well.
# Prepare
You should install Java on your platfrom: [jdk](https://www.oracle.com/technetwork/java/archive-139210.html)
# Install 
npm i fill-pdf-utf8-itext-promise
# Usage
``` javascript 
import {pdfFiller} from 'fill-pdf-utf8-itext-promise'
pdfFiller.generatePdf('source.pdf', 'dest.pdf', {fields: {name'姓名', age:'十九'}, {fontSize: 8.0})
```