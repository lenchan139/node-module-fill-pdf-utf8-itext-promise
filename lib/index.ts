var fill_pdf = require('fill-pdf-utf8');
export class pdfFiller {
    static generatePdf(sourcePdf: string, destPdf: string, dataOptions: any, styleOptions: any): Promise<any> {
        return new Promise((resolve, reject) => {

            fill_pdf.generatePdf(dataOptions, sourcePdf, styleOptions,destPdf, 
                (error:any, stdout: any, stderr:any)=>{
                if(error){
                    reject(error)
                }else{
                    resolve({
                        stdout: stdout,
                        stderr: stderr
                    })
                }
            })

        })
    }
}